#!/bin/bash

# Check if Python 3 is installed
python3_version=$(python3 -V 2>&1)
if [[ $python3_version == *"Python 3"* ]]; then
  echo "Python 3 is already installed with version: $python3_version"
else
  # Install Python 3 if not already installed
  echo "Python 3 is not installed, installing now..."
  sudo apt-get update
  sudo apt-get install python3 python3-pip -y
  echo "Python 3 is now installed with version: $(python3 -V 2>&1)"
fi

# Changing directory to /home/$USER/monitorscripts
mkdir -p ~/monitorscripts && cd ~/monitorscripts

# Getting the script from gitlab
wget https://gitlab.com/sangeeth.kavinthya/ova-automation-vmaas-it/-/raw/main/extract.py -O extract.py

# Change permissions
chmod 600 extract.py

# Run the script
python3 extract.py
