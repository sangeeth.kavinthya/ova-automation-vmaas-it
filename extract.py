import os

commands = {
        "HOSTNAME": "hostname",
        "IP Address": "ip route get 1.2.3.4 | awk '{print $7}'",
        "Storage Blocks": "lsblk",
        "Root Partition Usage": "df -Th /",
        "RAM Usage": "free -h",
        "OS Information": "cat /etc/os-release",
        "Hardware: Chassis vendor": "cat /sys/class/dmi/id/chassis_vendor",
        "Hardware: Product": "cat /sys/class/dmi/id/product_name",
    }

def getOutput(command):
    return os.popen(command).read().strip()

for cmd in commands:
    print('*'*5 + cmd + '*'*5)
    print(getOutput(commands[cmd]) + '\n\n')

